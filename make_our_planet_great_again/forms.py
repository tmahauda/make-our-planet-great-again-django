from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Button
from crispy_forms.bootstrap import FormActions


class ProjectForm(forms.Form):
    idea = forms.CharField(
        label='Idée',
        max_length=255,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        required=True
    )
    participants = forms.CharField(
        label='Participants',
        max_length=255,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        required=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'idea',
            'participants',
            FormActions(
                Submit('save', 'Sauvegarder'),
                Button('cancel', 'Annuler')
            )
        )
