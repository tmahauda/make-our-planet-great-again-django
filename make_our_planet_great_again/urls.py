"""make_our_planet_great_again URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from make_our_planet_great_again import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('project-holder/', include(('project_holder.urls', 'project-holder'), namespace='project-holder')),
    path('project-funder/', include(('project_funder.urls', 'project-funder'), namespace='project-funder')),
    path('project-evaluator/', include(('project_evaluator.urls', 'project-evaluator'), namespace='project-evaluator')),
    path('', views.index, name='index'),
    path('contact', views.contact, name='contact'),
    path('login', views.login, name='login'),
    path('mention_legales', views.mention_legales, name='mention-legales'),
    path('register', views.register, name='register'),
    path('forgot-password', views.forgot_password, name='forgot-password'),
]
