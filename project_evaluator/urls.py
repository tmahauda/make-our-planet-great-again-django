from django.urls import path
from project_evaluator import views

urlpatterns = [
    path('', views.index, name='index'),
    path('actionsEvaluateur/', views.actionsEvaluator, name='actionsEvaluateur'),
    path('updateEvalProject/', views.updateEvalProject, name='updateEvalProject'),
    path('viewEvalProject/', views.viewEvalProject, name='viewEvalProject'),
]
