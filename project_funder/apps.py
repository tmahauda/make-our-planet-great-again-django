from django.apps import AppConfig


class ProjectFunderConfig(AppConfig):
    name = 'project_funder'
