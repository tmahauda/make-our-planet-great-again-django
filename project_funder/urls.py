from django.urls import path
from project_funder import views

urlpatterns = [
    path('', views.index, name='index'),
    path('actionsFinanceur/', views.actionsFunder, name='actionsFinanceur'),
    path('updateFinProject/', views.updateFinProject, name='updateFinProject'),
    path('viewFinProject/', views.viewFinProject, name='viewFinProject'),
]
