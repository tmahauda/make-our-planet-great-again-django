from django.urls import path
from project_holder import views

urlpatterns = [
    path('', views.index, name='index'),
    path('addProject/', views.addproject, name='addProject'),
    path('actionsPorteur/', views.actionHolder, name='actionsPorteur'),
    path('viewPortProject/', views.viewPortProject, name='viewPortProject'),
    path('updatePortProject/', views.updatePortProject, name='updatePortProject'),
    path('deletePortProject/', views.deletePortProject, name='deletePortProject'),
]