from django.shortcuts import render


def index(request):
    return render(request, 'project-holder/index.html')


def addproject(request):
    return render(request, 'project-holder/addProject.html')


def actionHolder(request):
    return render(request, 'project-holder/actionsPorteur.html')


def viewPortProject(request):
    return render(request, 'project-holder/viewPortProject.html')


def updatePortProject(request):
    return render(request, 'project-holder/updatePortProject.html')


def deletePortProject(request):
    return render(request, 'project-holder/deletePortProject.html')
